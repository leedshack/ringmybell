"""
Listen to the microphone for a given frequency lasting for at least 0.5 seconds.
"""
import os
from datetime import datetime
import time
import struct

import pyaudio
import wave
import numpy as np
import requests

CHUNK = 1024  # bytes to read each time from mic
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000


def listen_for_freq(args, callback=lambda:True, freqs=(771, 772, 773, 1339, 1340, 1341), match=8):
    """
    Listens to Microphone for a given frequency,
    If 'match' in succession are detected, calls the callback function.
    """
    p = pyaudio.PyAudio()

    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK)

    print('')
    print("*" * 40)
    print("*{:^38}*".format('Doorbell Listener'))
    print("*{:^38}*".format('Team Monkey Tennis'))
    print("*{:^38}*".format('Leeds Hack 2016'))
    print("*" * 40)
    print("{} Listening to mic.".format(datetime.now()))
    if args.FAKE:
        print("Press space to stop, 5 to simulate doorbell sound...")
    window = np.blackman(CHUNK)
    swidth = 2

    cur_data = ''
    matched = 0
    while True:

        if args.FAKE:
            import sys, tty, termios
            fd = sys.stdin.fileno()
            old_settings = termios.tcgetattr(fd)
            try:
                tty.setraw(sys.stdin.fileno())
                ch = sys.stdin.read(1)
            finally:
                termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
            if ch == ' ':
                break

        cur_data = stream.read(CHUNK)
        # unpack the data and multiply by the hamming window
        indata = np.array(wave.struct.unpack("%dh" % (len(cur_data) / swidth), cur_data)) * window
        # Take the fft and square each value
        fftData = abs(np.fft.rfft(indata)) **2
        # find the max
        which = fftData[1:].argmax() + 1
        # use quadratic interpolation around the max
        if which != len(fftData) - 1:
            y0, y1, y2 = np.log(fftData[which-1:which+2:])
            x1 = (y2 - y0) * .5 / (2 * y1 - y2 - y0)
            # find the frequency
            thefreq = (which + x1) * RATE / CHUNK
            #print(thefreq)
            if int(thefreq) in freqs:
                matched += 1
                print("{} Hz".format(thefreq))
            else:
                matched = 0
        else:
            thefreq = which * RATE / CHUNK
            #print(thefreq)

        if args.FAKE and  ch == '5':
            matched = match + 1

        if matched > match:
            print("{} matched for {} sample chunks".format(datetime.now(), matched))
            # Note: we don't care if the callback takes time - this is a single-tasking doorbell!
            print("  < Callback returned {}".format(callback()))
            matched = 0

    print("Done")
    stream.close()
    p.terminate()
    return

def trigger():
    print("  > Notifying the server...")
    data = {'secret':os.environ['RING_MYBELL_SECRET'],
            'alert_type':'doorbell',
            'date_time':datetime.now(),
           }
    try:
        r = requests.post('http://ringmybell.tech:8000/letmein/', data, timeout=6)
        print("    " + r.content)
        return r.status_code
    except Exception as e:
        print(e)
        return False

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Listen for the doorbell')
    parser.add_argument('--test', dest='FAKE', action='store_true',
                        help='test without doorbell sound')
    parser.set_defaults(test=False)
    args = parser.parse_args()

    listen_for_freq(args, trigger)
