from django.contrib import admin

from .models import Visitor, Staff, AlertLog

admin.site.site_header = 'RingMyBell Administration'

admin.site.register(Visitor)
admin.site.register(Staff)
admin.site.register(AlertLog)
