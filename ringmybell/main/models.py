from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models


class Visitor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    access_code = models.IntegerField()
    photo = models.ImageField()

    def __unicode__(self):
        return self.user.get_full_name()

class Staff(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    on_call = models.BooleanField()
    phone = models.CharField(max_length=100, blank=True, null=True)
    sms = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Staff'

    def __unicode__(self):
        return unicode(self.user)

class AlertLog(models.Model):
    alert_type = models.CharField(max_length=25)
    date_time = models.DateTimeField()
    visitor = models.ForeignKey(Visitor, blank=True, null=True)

    def __unicode__(self):
        return "{} {}".format(self.date_time, self.alert_type)