# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-09 03:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20161009_0118'),
    ]

    operations = [
        migrations.AddField(
            model_name='alertlog',
            name='visitor',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='main.Visitor'),
            preserve_default=False,
        ),
    ]
