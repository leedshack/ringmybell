import os
from datetime import datetime

from django.conf import settings
from django.contrib import messages
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render

import do_ifttt
import do_twilio
from main.forms import AppAccess
from main.models import Visitor, Staff, AlertLog


class LetMeIn(FormView):

    SECRET = os.environ['RING_MYBELL_SECRET']

    ACTIONS = {
        'doorbell': 'doorbell_process',
        'app': 'app_process',
    }

    template_name = 'access.html'
    form_class = AppAccess

    def get_context_data(self, **kwargs):
        context = super(LetMeIn, self).get_context_data(**kwargs)
        context.update({'secret': self.SECRET})
        return context

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(LetMeIn, self).dispatch(request, *args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        secret = request.POST.get('secret')
        alert_type = request.POST.get('alert_type')
        if secret == self.SECRET:
            return getattr(self, self.ACTIONS.get(alert_type))(request, alert_type)
        else:
            resp = JsonResponse({'msg': 'Incorrect secret'})
        return resp

    def doorbell_process(self, request, alert_type):
        AlertLog.objects.create(alert_type = alert_type, date_time = timezone.now())
        do_ifttt.notify(value1=settings.IFTTT_URL)
        for s in Staff.objects.filter(on_call=True):
            if s.sms:
                do_twilio.notify(to=s.sms, body="There's somebody at the door")
        return JsonResponse({'msg': 'door bell process'})

    def app_process(self, request, alert_type):
        form = self.get_form()
        if form.is_valid():
            access_code = form.cleaned_data.get('access_code')
            visitor = Visitor.objects.get(access_code=access_code)
            AlertLog.objects.create(alert_type = alert_type, date_time = timezone.now(), visitor=visitor)
            do_ifttt.notify(value1=settings.IFTTT_URL)
            for s in Staff.objects.filter(on_call=True):
                if s.sms:
                    do_twilio.notify(to=s.sms, body="{} is at the door: https://ringmybell.tech/visitor/{}".format(
                        visitor.user.get_full_name(), visitor.access_code))
            message = ('Hello {}, a member of staff has been notified\
                    and will be with you shortly!').format(visitor.user.get_full_name())
            messages.success(request, message)
            return HttpResponseRedirect(reverse('success'))
        else:
            return self.form_invalid(form)


class ActivityFeed(TemplateView):
    template_name = 'activity.html'

    def activities(self):
        activities = AlertLog.objects.all().order_by('-date_time')
        paginator = Paginator(activities, 15)

        page = self.request.GET.get('page')
        try:
            activities = paginator.page(page)
        except PageNotAnInteger:
            activities = paginator.page(1)
        except EmptyPage:
            activities = paginator.page(paginator.num_pages)
        return activities

class VisitorView(TemplateView):
    template_name = 'visitor.html'

    def get(self, *args, **kwargs):
        access_code = args[1]
        context = self.get_context_data(**kwargs)
        try:
            visitor = Visitor.objects.get(access_code=access_code)
            context.update({'visitor': visitor})
        except:
            pass

        return self.render_to_response(context)
