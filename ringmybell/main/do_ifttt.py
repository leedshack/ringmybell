import os
import requests

def notify(*args, **kwargs):
    """Optional kwargs: value1, value2, value3"""
    data = dict(**kwargs)
    try:
        r = requests.post('https://maker.ifttt.com/trigger/doorbell/with/key/' + os.environ['IFTTT_KEY'], data, timeout=4)
        print("IFTTT event triggered: {}".format(r.content))
        return r.status_code
    except Exception as e:
        print(e)
        return False


