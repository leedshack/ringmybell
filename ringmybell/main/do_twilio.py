from twilio.rest import TwilioRestClient
from datetime import datetime
import os

from django.conf import settings
from django.utils import timezone

ACCOUNT_SID = os.environ["TWILIO_ACCOUNT_SID"]
AUTH_TOKEN = os.environ["TWILIO_AUTH_TOKEN"]

client = TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN)

def notify(to, body):
    try:
        if not settings.SMS_LIMITED or (settings.SMS_LIMITED[0] <= timezone.now().hour < settings.SMS_LIMITED[1]):
            r = client.messages.create(
                to = to,
                from_ = os.environ["TWILIO_FROM"],
                body = body,
            )
            print("SMS sent to {}: {}".format(to, r))
            return r
        else:
            print("SMS not sent to {} between {}".format(to, settings.SMS_LIMITED))
            return False
    except Exception as e:
        print(e)
        return False



