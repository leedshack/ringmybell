from django import forms
from main.models import Visitor


class AppAccess(forms.Form):
    access_code = forms.IntegerField()

    def clean_access_code(self):
        access_code = self.cleaned_data['access_code']
        visitor = Visitor.objects.filter(access_code=access_code)
        if not visitor:
            self.add_error('access_code', 'Invalid access code')
        return access_code
