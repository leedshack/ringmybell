# RingMyBell #

> Leeds Hack 2016

> Team **Monkey Tennis**

## Software Install ##

    sudo apt-get install portaudio19-dev

In a Python virtualenv:

    pip install -r ringmybell/requirements.txt

## Doorbell setup ##

Connect via usb cable.

Copy the `listener/1.mp3` file onto the device and make sure it is the selected melody.

## Listener ##

In the virtualenv:

    cd listener
    export RING_MYBELL_SECRET=<password>
    python listener.py

(ideally running on a Raspberry Pi with microphone and internet)

Place near the doorbell speaker.

## IFTTT ##

https://ifttt.com

Setup a trigger named `doorbell` and save the key in `IFTTT_KEY` below.

Use the trigger to send emails, post Slack messages etc.

## Twilio ##

https://www.twilio.com

Setup an account and save the account details below.

## Django Server ##

In the virtualenv:

    cd ringmybell
    export RING_MYBELL_SECRET=<password>
    export IFTTT_KEY=<key>
    export TWILIO_FROM=<from number>
    export TWILIO_ACCOUNT_SID=<sid>
    export TWILIO_AUTH_TOKEN=<auth>
    ./manage.py migrate  #one-off
    ./manage.py createsuperuser  #one-off

    ./manage.py runserver

